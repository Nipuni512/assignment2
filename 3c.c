#include <stdio.h>
int main()
{
  int n1,n2, temp;
  printf("Enter value for number 1: ");
  scanf("%d", &n1);
  printf("Enter value for number 2: ");
  scanf("%d", &n2);
  temp = n1;
  n1 = n2;
  n2 = temp;
  printf("Swapped Values: \nNum 1: %d \nNum 2: %d\n",n1,n2);
  return 0;
}
